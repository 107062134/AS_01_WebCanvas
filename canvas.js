var canvas = document.getElementById('art');
var ctx = canvas.getContext('2d');
var pen_active=0,tri_active=0,rec_active=0,cyc_active=0,line_active=0;
var ers_active=0,type_active=0;
var x_ori=0,y_ori=0;
var img;
var do_stack=[],redo_stack=[];

function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

function Pen(){
    pen_active=1;
    tri_active=0;
    rec_active=0;
    cyc_active=0;
    line_active=0;
    ers_active=0;
    type_active=0;
}
function Rect(){
    pen_active=0;
    tri_active=0;
    rec_active=1;
    cyc_active=0;
    line_active=0;
    ers_active=0;
    type_active=0;
}
function Cycle(){
    pen_active=0;
    tri_active=0;
    rec_active=0;
    cyc_active=1;
    line_active=0;
    ers_active=0;
    type_active=0;
}
function Tri(){
    pen_active=0;
    tri_active=1;
    rec_active=0;
    cyc_active=0;
    line_active=0;
    ers_active=0;
    type_active=0;
}
function Line(){
    pen_active=0;
    tri_active=0;
    rec_active=0;
    cyc_active=0;
    line_active=1;
    ers_active=0;
    type_active=0;
}
function Erase(){
    pen_active=0;
    tri_active=0;
    rec_active=0;
    cyc_active=0;
    line_active=0;
    ers_active=1;
    type_active=0;
}


function mouseMove(evt) {
  var mousePos = getMousePos(canvas, evt);
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();
}
function mouseMove2(evt) {//pic
    var mousePos = getMousePos(canvas, evt);
    ctx.putImageData(img,0,0);
    ctx.beginPath();
    if(rec_active==1)ctx.strokeRect(x_ori, y_ori,mousePos.x-x_ori,mousePos.y-y_ori);
    else if(line_active==1){
        ctx.moveTo(x_ori,y_ori);
        ctx.lineTo(mousePos.x,mousePos.y);
        ctx.stroke();
    }
    else if (tri_active==1){
        ctx.moveTo(x_ori,y_ori);
        if((x_ori-mousePos.x)*(x_ori-mousePos.x)>(y_ori-mousePos.y)*(y_ori-mousePos.y)){
            ctx.lineTo(mousePos.x,mousePos.y);
            ctx.lineTo(2*x_ori-mousePos.x,mousePos.y);
            ctx.lineTo(x_ori,y_ori);
        }
        else{
            ctx.lineTo(mousePos.x,mousePos.y);
            ctx.lineTo(mousePos.x,2*y_ori-mousePos.y);
            ctx.lineTo(x_ori,y_ori);
        }
        ctx.stroke();
    }
    else if(cyc_active==1){
        //ctx.moveTo(x_ori,y_ori);
        ctx.arc(mousePos.x,mousePos.y,Math.sqrt((x_ori-mousePos.x)*(x_ori-mousePos.x)+(y_ori-mousePos.y)*(y_ori-mousePos.y)),0,Math.PI*2,true)
        ctx.stroke();
    }

}
function mouseMove3(evt) {
    var mousePos = getMousePos(canvas, evt);
    ctx.globalCompositeOperation = 'destination-out';
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();
}
canvas.addEventListener('mousedown', function(evt) {
    var mousePos = getMousePos(canvas, evt);
    ctx.beginPath();
    img = ctx.getImageData(0,0,500,500);
    do_stack.push(ctx.getImageData(0,0,500,500));
    ctx.globalCompositeOperation = 'source-over';
    if(ers_active==1){
        ctx.moveTo(mousePos.x, mousePos.y);
        canvas.addEventListener('mousemove', mouseMove3, false);
        canvas.addEventListener('mouseup', function() {
            canvas.removeEventListener('mousemove', mouseMove3, false);
        }, false);
    }
    else if(pen_active==1){
        ctx.moveTo(mousePos.x, mousePos.y);
        canvas.addEventListener('mousemove', mouseMove, false);
        canvas.addEventListener('mouseup', function() {
            canvas.removeEventListener('mousemove', mouseMove, false);
        }, false);
    }
    else if(tri_active==1||rec_active==1||cyc_active==1||line_active==1){
        ctx.moveTo(mousePos.x, mousePos.y);
        x_ori=mousePos.x;
        y_ori=mousePos.y;
        canvas.addEventListener('mousemove', mouseMove2, false);
        canvas.addEventListener('mouseup', function() {
            canvas.removeEventListener('mousemove', mouseMove2, false);
        }, false);
    }
});

//canvas.addEventListener('mouseup', function() {
  //canvas.removeEventListener('mousemove', mouseMove, false);
//}, false);


document.getElementById('reset').addEventListener('click', function() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}, false);

/*function Color(){
    var cc=document.getElementById('color').value;
    ctx.strokeStyle = cc;
}*/
var theInput = document.getElementById("color");
var theColor = theInput.value;

theInput.addEventListener("change", function() {
    ctx.strokeStyle=theInput.value;
}, false);

function Size(){
    var ss=document.getElementById('range').value;
    ctx.lineWidth=ss;
}

var download = function(){
    var link = document.createElement('a');
    link.download = 'pic.png';
    link.href = document.getElementById('art').toDataURL()
    link.click();
}
$('#upload').change(function () {
    
    var img = new Image();
    
    img.onload = function () {
        canvas.width = this.width
        canvas.height = this.height
        ctx.drawImage(this, 0, 0, canvas.width, canvas.height)
        URL.revokeObjectURL(src)
    }
    
    var file = this.files[0];
    var src = URL.createObjectURL(file);
    
    img.src = src;
});

function Undo(){
    redo_stack.push(ctx.getImageData(0,0,500,500));
    var img2=do_stack.pop();
    ctx.putImageData(img2,0,0);
}
function Redo(){
    do_stack.push(ctx.getImageData(0,0,500,500));
    var img3=redo_stack.pop();
    ctx.putImageData(img3,0,0);
}